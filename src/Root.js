import React, { Component } from "react";
import { Card } from 'material-ui';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AnimatedBarChart from './AnimatedBarChart';
import normalizeData from './util/normalizePigData';
import isMobile from './util/isMobile';
import pigData from './data/wild-pig-data.json';
import './Root.css';
const data = normalizeData(pigData);

const WIDTH = isMobile ? 350 : 700;
const HEIGHT = isMobile ? 400 : 500;

export default class Root extends Component {
  render() {
    return (
      <MuiThemeProvider>
        <div className="Root" style={{ width: WIDTH, marginTop: 20 }}>
          <Card>
            <AnimatedBarChart title="Hawaiian Island Pig Population" data={data} width={WIDTH} height={HEIGHT} />
          </Card>
        </div>
      </MuiThemeProvider>
    );
  }
}

