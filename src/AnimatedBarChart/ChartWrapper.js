// @flow
import type { LocationDatum } from '../util/types';
import { extent } from 'd3-array';
import { getQueryParam, setQueryParam } from '../util/urlQuerys';
import React, { Component } from "react";
import { RaisedButton, LinearProgress, Subheader } from 'material-ui';
import Chart from './Chart';
import { lightBlue500 } from 'material-ui/styles/colors';
import margins from './chartMargins';
// TODO: Support dynamic date ranges - btd
const MAX_YEAR = 2005;
const MIN_YEAR = 2000;
const TOOL_BAR_HEIGHT = 50; // The height of the tool bar area, which is seperate from the chart view port.
const toolbarStyle = { marginLeft: 10, marginTop: 10 };

type Props = {
  data: LocationDatum[],
  width: number,
  height: number,
  title: string,
};
type State = {
  isPaused: boolean,
  year: number
};

const INCREMENT_INTERVAL = 2000; // In milliseconds - the time between incrementing the year.
const style = { margin: 12, marginTop: 4 };

export default class ChartWrapper extends Component<Props, State> {
  static defaultProps = {
    width: 500,
    height: 400,
  }

  state = {
    isPaused: getQueryParam('paused') === 'true',
    year: getQueryParam('year') != null ? Number(getQueryParam('year')) : MIN_YEAR,
  }

  intervalId : ?any = null;

  componentDidMount() {
    if (!this.state.isPaused) {
      this.enableTimer();
    }
  }

  componentWillUnmount() {
    this.disableTimer();
  }

  enableTimer() {
    const intervalId = setInterval(this.incrementTimer, INCREMENT_INTERVAL);
    this.intervalId = intervalId;
  }

  disableTimer() {
    if (this.intervalId != null) {
      clearInterval(this.intervalId);
    }
  }

  incrementTimer = () => {
    const year = this.state.year + 1;
    if (year > MAX_YEAR) {
      this.reset();
    } else {
      this.setState({ year });
      window.location.href = setQueryParam('year', year);
    }
  }

  togglePause = () => {
    const isPaused = !this.state.isPaused;
    window.location.href = setQueryParam('paused', String(isPaused));
    this.setState({ isPaused });
    if (!isPaused) {
      this.enableTimer();
    } else {
      this.disableTimer();
    }
  }

  reset = () => {
    if (this.state.isPaused) {
      this.togglePause();
    }
    this.setState({ year: MIN_YEAR });
    window.location.href = setQueryParam('year', MIN_YEAR);
  }

  render() {
    const { year, isPaused } = this.state;
    const { data, width, height, title } = this.props;
    const valueRange = extent(data.map(d => d.value)); // Returns [min, max] with respect to the range of values.
    const viewData = data
      .filter(d => d.year === year) // Show only the active year
      .sort((a, b) => a.location.localeCompare(b.location)); // We want the order to remain the same regardless of data order
    return [
      <Subheader style={{ textAlign: 'center' }} key="0">
        {title} for year {year}
      </Subheader>,
      <Chart
        key="1"
        data={viewData}
        minValue={valueRange[0]}
        maxValue={valueRange[1]}
        width={width}
        height={height - TOOL_BAR_HEIGHT}
      />,
      <LinearProgress
        key="2"
        color={lightBlue500}
        style={{ width: width - margins.left - margins.right - 10, marginLeft: margins.left }}
        mode="determinate"
        min={MIN_YEAR}
        max={MAX_YEAR}
        value={year}
      />,
      <div style={toolbarStyle} key="3">
        <RaisedButton
          primary
          style={style}
          label={isPaused ? 'Play' : 'Pause'}
          onClick={this.togglePause}
        />
        <RaisedButton
          label="Reset"
          onClick={this.reset}
          style={style}
        />
      </div>,
    ];
  }
}
