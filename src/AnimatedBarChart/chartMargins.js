// @flow
/**
 * Chart Margins
 *
 * Control the margins within the chart viewport.
 */
// TODO: top and right margin don't work currently -btd
const margins = {
  left: 30,
  bottom: 32,
  top: 0,
  right: 0,
};

export default margins;
