// @flow
import type { LocationDatum } from '../util/types';
import React, { Component } from 'react';
import { scaleLinear } from 'd3-scale';
import { lightBlue500, darkBlack } from 'material-ui/styles/colors';
import margins from './chartMargins';
const barStyle = { fill: lightBlue500 };
const textStyle = { color: darkBlack, fontSize: 12 };

type Props = {
  data: LocationDatum[],
  height: number,
  width: number,
  minValue: number,
  maxValue: number,
}
const GAP_PERCENT = 0.3; // Control the percentage of the width between values is margins. higher -> larger margins

export default class Chart extends Component<Props> {
  render() {
    const { width, height, data, maxValue } = this.props;
    /**
     * Declare Axis Scales
     */
    const xScale = scaleLinear()
      .domain([0, data.length])
      .range([0, width - margins.left]);
    const yScale = scaleLinear()
      .domain([1, maxValue])
      .range([0, (height - margins.bottom) * 0.80]);

    return (
      <svg
        width={width}
        height={height}
      >
        <g transform={`translate(${margins.left} ${-margins.bottom})`}>
          {data.map(({ location, value }, i : number) => {
            const barHeight = yScale(value);
            const drawTextInBar = value > 1200;
            const topY = height - margins.bottom - barHeight + (drawTextInBar ? 15 : -5);
            return [
              <rect
                key={location}
                x={xScale(i)}
                y={height - margins.bottom - barHeight}
                height={barHeight}
                width={xScale(1 - GAP_PERCENT)}
                style={barStyle}
              />,
              <text
                textAnchor="middle"
                key={`${location}-value`}
                x={xScale(i + (1 - GAP_PERCENT) / 2)}
                y={topY}
                style={{ color: darkBlack, fontSize: 12 }}
              >
                {value >= 1000 ? `${Math.round(value / 1000)}k` : value}
              </text>,
              <g
                transform={`translate(${xScale(i + (1 - GAP_PERCENT) / 2 + 0.15)}, ${height - margins.bottom + 10})`}
                key={`${location}-label`}
              >
                <text
                  textAnchor="end"
                  transform="rotate(-45)"
                  style={textStyle}
                >
                  {location}
                </text>
              </g>,
            ]
          })}
        </g>
      </svg>
    )
  }
}