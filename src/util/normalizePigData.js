// @flow
import type { LocationDatum } from './types';

type PigDatum = {
  pigPopulation: number,
  island: string,
  year: number,
};

type PigData = {
  'PIG POPULATIONS': PigDatum[],
};

/**
 * Normalize Pig Data
 *
 * Map the given data format into a more generalized data format.
 * This allows us to design components to consume data from a variety of sources as long as it fits the format
 * of one value per location per year.
 */
const normalizeData = (wrapper : PigData) : LocationDatum[] => wrapper['PIG POPULATIONS'].map(({ year, island, pigPopulation }) => ({
  year,
  location: island,
  value: pigPopulation,
}));
export default normalizeData;
