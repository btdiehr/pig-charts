// @flow
export type LocationDatum = { value: number, location: string, year: number }