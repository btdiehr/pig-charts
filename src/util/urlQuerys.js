// @flow
/**
 * Url Query Utilities
 */

/**
 * Get Query Param
 *
 * Returns the value of the requested query param.
 *
 * source: https://stackoverflow.com/questions/901115/how-can-i-get-query-string-values-in-javascript
 */
export const getQueryParam = (name : string) : string | null => {
  const url = window.location.href;
  const cleanedName = name.replace(/[[\]]/g, '\\$&');
  const regex = new RegExp(`[?&]${cleanedName}(=([^&#]*)|&|#|$)`);
  const results = regex.exec(url);
  if (!results) {
    return null;
  } else if (!results[2]) {
    return '';
  } else {
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
  }
};

/**
 * Set Query Param
 *
 * Update the query param of the uri by key value pairs.
 * Source: https://stackoverflow.com/questions/5999118/how-can-i-add-or-update-a-query-string-parameter
 */
// TODO: Clean this up! - btd
export const setQueryParam = (key : string, value : string | number) : string => {
  const uri = window.location.href;
  const re = new RegExp("([?&])" + key + "=.*?(&|#|$)", "i");
  if (value === undefined) {
    if (uri.match(re)) {
      return uri.replace(re, '$1$2');
    } else {
      return uri;
    }
  } else {
    if (uri.match(re)) {
      return uri.replace(re, '$1' + key + "=" + value + '$2');
    } else {
      const separator = uri.indexOf('?') !== -1 ? "&" : "?";
      const addHash = uri.indexOf('#') === -1;
      if (addHash) {
        return uri + '#/' + separator + key + "=" + value;
      } else {
        return uri + separator + key + "=" + value;
      }
    }
  }
}